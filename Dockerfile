FROM maven:3.8.6-jdk-8

WORKDIR /app

RUN useradd -u 2002 my_none_root_user

COPY . .

RUN mvn clean package && \
    apt-get install git -y && \
    git_commit_date=$(git log -n1 --pretty=format:'%cd' --date=format:'%y%m%d') && \
    git_commit_sha=$(git rev-parse --short HEAD) && \
    cd target && \
    mv assignment-*.jar ${git_commit_date}-${git_commit_sha}.jar

COPY ./target/*.jar app/target/*.jar

EXPOSE 8080

ENV SPRING_PROFILES_ACTIVE="mysql" \
    JAVA_OPTS="-Xmx1g -Xms512m"
 
USER my_none_root_user

CMD ["java", "-jar", "-Dspring.profiles.active=${SPRING_PROFILES_ACTIVE}", "-Djava.awt.headless=true", "app/target/*.jar"]